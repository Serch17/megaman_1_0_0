move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_wall)){
    vsp = key_jump * -jumpspeed;
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_wall)){
    while(!place_meeting(x+sign(hsp),y,obj_wall))    {
        x+= sign(hsp);
    }
    hsp = 0;
    hcollision = true;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_wall)){
    while(!place_meeting(x, sign(vsp) + y,obj_wall))    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;

